# Sample Systemd Unit File

You may wish to have systemd manage rostrum so that it's "always on." Here is a sample unit file (which assumes that the bitcoind unit file is `bitcoind.service`):

```
[Unit]
Description=Rostrum
After=bitcoind.service

[Service]
WorkingDirectory=/home/bitcoin/rostrum
ExecStart=/home/bitcoin/rostrum/target/release/rostrum --db-dir ./db --electrum-rpc-addr="127.0.0.1:50001"
User=bitcoin
Group=bitcoin
Type=simple
KillMode=process
TimeoutSec=60
Restart=always
RestartSec=60

[Install]
WantedBy=multi-user.target
```