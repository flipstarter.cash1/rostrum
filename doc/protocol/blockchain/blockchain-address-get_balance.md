## blockchain.address.get\_balance

Return the confirmed and unconfirmed balances of a Bitcoin Cash address.

**Signature**

> Function:
> blockchain.address.get\_balance(address, filter="include\_tokens")
>
> Version added: 1.4.3
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Legacy addresses (base58) are also supported.
>
> -   *filter*
>
>     Filter what utxos are included in the query. Valid filters are:
>
>     - include\_tokens - Include all utxos
>     - tokens\_only - Only include token utxos
>     - exclude\_token - Only include utxos without tokens
>

**Result**

> See [blockchain.scripthash.get_balance](/protocol/blockchain/blockchain-address-get_balance.md).
