## blockchain.transaction.broadcast

Broadcast a transaction to the network.

**Signature**

> Function:
> blockchain.transaction.broadcast(raw\_tx)
>
> Version changed:
> 1.1 errors returned as JSON RPC errors rather than as a result.
>
> *raw_tx*
>
> > The raw transaction as a hexadecimal string.

**Result**

> The transaction hash as a hexadecimal string.
>
> **Note** protocol version 1.0 (only) does not respond according to the
> JSON RPC specification if an error occurs. If the daemon rejects the
> transaction, the result is the error message string from the daemon,
> as if the call were successful. The client needs to determine if an
> error occurred by comparing the result to the expected transaction
> hash.

**Result Examples**

    "a76242fce5753b4212f903ff33ac6fe66f2780f34bdb4b33b175a7815a11a98e"

Protocol version 1.0 returning an error as the result:

    "258: txn-mempool-conflict"
