## blockchain.address.get\_first\_use

Returns a first occurance of usage of scripthash as ouput on the blockchain.

See `blockchain.scripthash.get_first_use`.

**Signature**

>  Function: blockchain.scripthash.get\_first\_use(address, filter="include\_tokens")
>
>  Version added: Added in Rostrum 1.2 (previously named ElectrsCash)
>
>  * *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Legacy addresses (base58) are also supported.
>
> -   *filter*
>
>     Filter what utxos are included in the query. Valid filters are:
>
>     - include\_tokens - Include all utxos
>     - tokens\_only - Only include token utxos
>     - exclude\_token - Only include utxos without tokens

**Result**

> See [blockchain.scripthash.get_first_use](/protocol/blockchain/blockchain-scripthash-get_first_use.md).