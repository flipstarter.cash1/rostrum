## blockchain.address.listunspent

Return an ordered list of UTXOs sent to a Bitcoin Cash or Nexa address.

**Signature**

> Function:
> blockchain.address.listunspent(address, filter="include\_tokens")
>
> Version added:
> 1.4.3
>
> -   *address*
>
>     The address as a Cash Address string (with or without prefix).
>     Legacy addresses (base58) are also supported.
>
> -   *filter*
>
>     Filter what utxos are included in the query. Valid filters are:
>
>     - include\_tokens - Include all utxos
>     - tokens\_only - Only include token utxos
>     - exclude\_token - Only include utxos without tokens

**Result**

> As for [blockchain.scripthash.listunspent](/protocol/blockchain/blockchain-address-listunspent.md)