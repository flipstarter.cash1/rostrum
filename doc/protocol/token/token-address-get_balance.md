## token.address.get\_balance

Return the confirmed and unconfirmed balances of tokens in a Bitcoin Cash or Nexa address.

**Signature**

> Function:
> token.address.get\_balance(address, cursor *(optional)*, token *(optional)*)
>
> Version added: Rostrum 6.0
>
> -   *address*
>
>     The address as a Nexa or Cash Address string (with or without prefix).
>
> -   *cursor*
>
>     If the address has a large amount of tokens, the results may be paginated. The cursor is `null` if this result was the
>     last (or the full) result set. If there are more results to
>     fetch, call the method again with this cursor value to get next set of results.
>
> -   *token (optional)*
>
>     Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.

**Result**

> See `token.scripthash.get_balance`.