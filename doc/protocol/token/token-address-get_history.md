## token.address.get\_history

Return the confirmed and unconfirmed token history of a Nexa or Bitcoin Cash address.

**Signature**

> Function:
> token.address.get\_history(address, cursor *(optional)*, token *(optional)*)
>
> Version added:
> Rostrum 6.0
>
> -   *address*
>
>     The address as a Nexa or Cash Address string (with or without prefix).

>
> -   *cursor (optional)*
>
>     Null or string. If null, indicates that all histoy is requested, from newest entry first. If a cursor
>     is provided from an earlier call, history is continued from last transaction in earlier call.
>
> -   *token (optional)*
>
>     Token ID as cashaddr encoded or hexadecimal string. If provided, will filter out transactions that do not include token.


**Result**

> See `token.scripthash.get_history`