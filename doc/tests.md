# Tests & Testing

## Unit tests
Run rostrum unit tests with `cargo test`.

## Functional tests

Rostrum has a large set of functional tests that run in CI against multiple full nodes; BCHUnlimited, Nexa and BCHN.

The functional tests are located in the `test/functional`folder of the rostrum repo. They can be started individually or in batch by running
`test/functional/test_runner.py`. The tests look for node software in environment variable `NODE_PATH` and rostrum in `ROSTRUM_PATH`.

In addition to being run in this repo to ensure no breaking changes, the tests are also run as part of Bitcoin Unlimited CI for nodes BCHUnlimited
and Nexa to ensure any changes added there does not break rostrum integration.

### Run tests from Bitcoin Unlimited

Clone and compile the [Bitcoin Unlimited](https://gitlab.com/bitcoinunlimited/BCHUnlimited) node software.

Start the electrum tests with `./qa/pull-tester/rpc-tests.py --electrum.exec="$HOME/rostrum/target/debug/rostrum" --electrum-only`.

The `--electrum.exec` argument is the path to the rostrum binary. The `--electrum-only` parameter is to skip any non-electrum integration test.
