# Bitcoind configuration

Rostrum requires a full node to query for data and network status. This document covers how to connect and authenticate with a full node.

## Bitcoin Unlimited

Rostrum is automatically configured and started with Bitcoin Unlimited. If the binary is not in the same directory as `bitcoind`, you can specify the alternative location using `-electrum.exec=/full/path/rostrum`.

For more options, see `bitcoind -help`.

## Other nodes

If you are using `-rpcuser=USER` and `-rpcpassword=PASSWORD` for authentication, please use `auth="USER:PASSWORD"` option in one of the config files.

Otherwise, `~/.bitcoin/.cookie` will be read, allowing this server to use bitcoind JSONRPC interface.