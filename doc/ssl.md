# SSL Support

In order to use a secure connection, you can also use [NGINX as an SSL endpoint](https://docs.nginx.com/nginx/admin-guide/security-controls/terminating-ssl-tcp/#) by placing the following block in `nginx.conf`.

## Configuration example
### For Bitcoin Cash

This configures SSL and WSS (websocket ssl) for Bitcoin Cash mainnet. For
other networks, find the correct port in [default ports document](ports.md)

```nginx
stream {
        upstream rostrum {
                server 127.0.0.1:50001;
        }

        server {
                listen 50002 ssl;
                proxy_pass rostrum;

                ssl_certificate /path/to/example.crt;
                ssl_certificate_key /path/to/example.key;
                ssl_session_cache shared:SSL:1m;
                ssl_session_timeout 4h;
                ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
                ssl_prefer_server_ciphers on;
        }

        upstream rostrum_ws {
            server 127.0.0.1:50003;
        }
        server {
                listen 50004 ssl;
                proxy_pass rostrum_ws;

                ssl_certificate /path/to/example.crt;
                ssl_certificate_key /path/to/example.key;
                ssl_session_cache shared:SSL:1m;
                ssl_session_timeout 4h;
                ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
                ssl_prefer_server_ciphers on;
        }
}
```

### For Nexa

This configures SSL and WSS (websocket ssl) for Nexa mainnet. For
other networks, find the correct port in [default ports document](ports.md)

```nginx
stream {
        upstream rostrum {
                server 127.0.0.1:20001;
        }

        server {
                listen 20002 ssl;
                proxy_pass rostrum;

                ssl_certificate /path/to/example.crt;
                ssl_certificate_key /path/to/example.key;
                ssl_session_cache shared:SSL:1m;
                ssl_session_timeout 4h;
                ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
                ssl_prefer_server_ciphers on;
        }

        upstream rostrum_ws {
            server 127.0.0.1:20003;
        }
        server {
                listen 20004 ssl;
                proxy_pass rostrum_ws;

                ssl_certificate /path/to/example.crt;
                ssl_certificate_key /path/to/example.key;
                ssl_session_cache shared:SSL:1m;
                ssl_session_timeout 4h;
                ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3;
                ssl_prefer_server_ciphers on;
        }
}
```

## Running and testing

```bash
$ sudo systemctl restart nginx
$ electron-cash --oneserver --server=example:50002:s
```

##  SSL cerficiate
Note: If you are connecting to rostrum a client which does not allow self-signed SSL certificates, you can obtain a free SSL certificate as follows:

1. Follow the instructions at https://certbot.eff.org/ to install the certbot on your system.
2. When certbot obtains the SSL certificates for you, change the SSL paths in the nginx template above as follows:
```
ssl_certificate /etc/letsencrypt/live/<your-domain>/fullchain.pem;
ssl_certificate_key /etc/letsencrypt/live/<your-domain>/privkey.pem;
```
