use crate::store::Row;
use bitcoin_hashes::Hash;
use bitcoincash::Txid;
use sha2::{Digest, Sha256};

use super::DBRow;

const CASHACCOUNT_CF: &str = "cashaccount";

fn compute_accountname_hash(accountname: &[u8], blockheight: u32) -> [u8; 32] {
    let mut sha2 = Sha256::new();
    sha2.update(accountname);
    sha2.update(blockheight.to_be_bytes());
    sha2.finalize().into()
}

#[derive(Serialize, Deserialize)]
pub struct TxCashAccountKey {
    account_hash: [u8; 32],
}

#[derive(Serialize, Deserialize)]
pub struct TxCashAccountRow {
    key: TxCashAccountKey,
    txid: [u8; 32],
}

impl TxCashAccountRow {
    pub fn new(txid: &Txid, accountname: &[u8], blockheight: u32) -> TxCashAccountRow {
        TxCashAccountRow {
            key: TxCashAccountKey {
                account_hash: compute_accountname_hash(accountname, blockheight),
            },
            txid: txid.into_inner(),
        }
    }

    pub fn filter(accountname: &[u8], blockheight: u32) -> Vec<u8> {
        bincode::serialize(&TxCashAccountKey {
            account_hash: compute_accountname_hash(accountname, blockheight),
        })
        .unwrap()
    }

    pub fn txid(&self) -> Txid {
        Txid::from_inner(self.txid)
    }
}

impl DBRow for TxCashAccountRow {
    const CF: &'static str = CASHACCOUNT_CF;

    fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self).unwrap().into_boxed_slice(),
            value: vec![].into_boxed_slice(),
        }
    }

    fn from_row(row: &Row) -> TxCashAccountRow {
        bincode::deserialize(&row.key).expect("failed to parse TxCashAccountRow")
    }
}
