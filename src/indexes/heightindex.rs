use crate::store::Row;
use bitcoin_hashes::Hash;
use bitcoincash::Txid;

use super::DBRow;

const HEIGHTINDEX_CF: &str = "height";

#[derive(Serialize, Deserialize, Debug)]
pub struct HeightIndexKey {
    pub txid: [u8; 32],
}

#[derive(Debug)]
pub struct HeightIndexRow {
    pub key: HeightIndexKey,
    height: u32,
}

impl HeightIndexRow {
    pub fn new(txid: &Txid, height: u32) -> HeightIndexRow {
        HeightIndexRow {
            key: HeightIndexKey {
                txid: txid.into_inner(),
            },
            height,
        }
    }

    pub fn filter_by_txid(txid: &Txid) -> Vec<u8> {
        txid.to_vec()
    }

    pub fn txid(&self) -> Txid {
        Txid::from_inner(self.key.txid)
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    /**
     * For fetching the height without deserializing the full row object.
     */
    pub fn height_from_dbvalue(value: &[u8]) -> u32 {
        bincode::deserialize(value).expect("failed to deserialize height")
    }
}

impl DBRow for HeightIndexRow {
    const CF: &'static str = HEIGHTINDEX_CF;
    fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self.key).unwrap().into_boxed_slice(),
            value: bincode::serialize(&self.height).unwrap().into_boxed_slice(),
        }
    }

    fn from_row(row: &Row) -> HeightIndexRow {
        HeightIndexRow {
            key: bincode::deserialize(&row.key).expect("failed to parse TxKey"),
            height: bincode::deserialize(&row.value).expect("failed to parse height"),
        }
    }
}
