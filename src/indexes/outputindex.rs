use crate::chaindef::OutPointHash;
use crate::store::Row;
use bitcoincash::hashes::Hash;
use bitcoincash::Txid;

use super::DBRow;

/**
 * Index of utxos on the blockchain.
 */

const OUTPUTINDEX_CF: &str = "output";

#[derive(Serialize, Deserialize, Debug)]
pub struct OutputIndexKey {
    pub hash: [u8; 32],
}

#[derive(Serialize, Deserialize, Debug)]
pub struct OutputIndexRow {
    key: OutputIndexKey,
    txid: [u8; 32],
    index: u32,
    value: u64,
}

impl OutputIndexRow {
    pub fn new(
        txid: &Txid,
        outpoint_hash: &OutPointHash,
        output_value: u64,
        output_index: u32,
    ) -> OutputIndexRow {
        OutputIndexRow {
            key: OutputIndexKey {
                hash: outpoint_hash.into_inner(),
            },
            txid: txid.into_inner(),
            index: output_index,
            value: output_value,
        }
    }

    pub fn filter_by_outpointhash(hash: &OutPointHash) -> Vec<u8> {
        bincode::serialize(&OutputIndexKey {
            hash: hash.into_inner(),
        })
        .unwrap()
    }

    pub fn hash(&self) -> OutPointHash {
        OutPointHash::from_inner(self.key.hash)
    }

    pub fn take_hash(self) -> OutPointHash {
        OutPointHash::from_inner(self.key.hash)
    }

    pub fn txid(&self) -> Txid {
        Txid::from_inner(self.txid)
    }

    pub fn index(&self) -> u32 {
        self.index
    }

    pub fn value(&self) -> u64 {
        self.value
    }
}

impl DBRow for OutputIndexRow {
    const CF: &'static str = OUTPUTINDEX_CF;
    fn to_row(&self) -> Row {
        Row {
            key: bincode::serialize(&self.key).unwrap().into_boxed_slice(),
            value: bincode::serialize(&(self.txid, self.index, self.value))
                .unwrap()
                .into_boxed_slice(),
        }
    }

    fn from_row(row: &Row) -> OutputIndexRow {
        let (txid, index, value): ([u8; 32], u32, u64) =
            bincode::deserialize(&row.value).expect("failed to parse OutputIndexRow");
        OutputIndexRow {
            key: bincode::deserialize(&row.key).expect("failed to parse OutputIndexKey"),
            txid,
            index,
            value,
        }
    }
}
