use anyhow::{Context, Result};
use serde_json::Value;
use std::{
    collections::hash_map::DefaultHasher,
    hash::Hasher,
    net::{IpAddr, SocketAddr, TcpStream},
    time::{Duration, Instant},
};

// Connection timeout until considering server peer bad
const CANDIDATE_TIMEOUT: Duration = Duration::from_secs(10);

pub trait ServerPeer {
    /// Every implementation of this trait must produce
    /// the same ID, given the same data.
    fn id(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        if let Some(p) = self.tcp_port() {
            hasher.write_u16(p)
        };
        if let Some(p) = self.ssl_port() {
            hasher.write_u16(p)
        };
        hasher.write(self.ip().to_string().as_bytes());
        hasher.finish()
    }
    fn tcp_port(&self) -> Option<u16>;
    fn ssl_port(&self) -> Option<u16>;
    fn ip(&self) -> IpAddr;
    fn hostname(&self) -> String;
}

impl std::fmt::Display for dyn ServerPeer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Strip weird utf-8 characters.
        let safe_hostname = self
            .hostname()
            .chars()
            .filter(|c| c.is_ascii())
            .collect::<String>();
        write!(
            f,
            "Peer {} - IP: {} TCP: {} SSL: {}",
            safe_hostname,
            self.ip(),
            self.tcp_port().unwrap_or_default(),
            self.ssl_port().unwrap_or_default()
        )
    }
}

/**
 * A server that has announced itself to us, but we have not verified.
 */
#[derive(Debug, Clone)]
pub struct CandidatePeer {
    pub(crate) tcp_port: Option<u16>,
    pub(crate) ssl_port: Option<u16>,
    pub(crate) hostname: Option<String>,
    pub(crate) ip: IpAddr,
}

impl ServerPeer for CandidatePeer {
    fn tcp_port(&self) -> Option<u16> {
        self.tcp_port
    }

    fn ssl_port(&self) -> Option<u16> {
        self.ssl_port
    }

    fn ip(&self) -> IpAddr {
        self.ip
    }

    fn hostname(&self) -> String {
        match self.hostname.as_ref() {
            Some(h) => h.clone(),
            None => self.ip.to_string(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct GoodKnownPeer {
    tcp_port: Option<u16>,
    ssl_port: Option<u16>,
    hostname: String,
    ip: IpAddr,
    /// Last time we checked if this was a good peer.
    pub last_visit: Instant,
    /// Last time we asked it for server peer candidates
    pub last_candidate_request: Instant,
}

impl ServerPeer for GoodKnownPeer {
    fn tcp_port(&self) -> Option<u16> {
        self.tcp_port
    }

    fn ssl_port(&self) -> Option<u16> {
        self.ssl_port
    }

    fn ip(&self) -> IpAddr {
        self.ip
    }

    fn hostname(&self) -> String {
        self.hostname.clone()
    }
}

impl From<CandidatePeer> for GoodKnownPeer {
    fn from(val: CandidatePeer) -> GoodKnownPeer {
        let host = match val.hostname {
            Some(h) => h,
            None => val.ip.to_string(),
        };
        GoodKnownPeer {
            tcp_port: val.tcp_port,
            ssl_port: val.ssl_port,
            hostname: host,
            ip: val.ip,
            last_visit: Instant::now(),
            last_candidate_request: Instant::now()
                .checked_sub(Duration::from_secs(600))
                .unwrap(),
        }
    }
}

/**
 * Connects to TCP port of an electrum server
 */
pub fn connect_to_electrum_server(peer: &dyn ServerPeer) -> Result<TcpStream> {
    trace!(
        "Connecting to electrum server {} (IP: {})",
        peer.hostname(),
        peer.ip()
    );

    let port = match peer.tcp_port() {
        Some(p) => p,
        None => {
            bail!("SSL NYI")
        }
    };

    let stream = TcpStream::connect_timeout(&SocketAddr::new(peer.ip(), port), CANDIDATE_TIMEOUT)
        .context("Failed to connect")?;
    stream
        .set_read_timeout(Some(CANDIDATE_TIMEOUT))
        .context("Failed to set read timeout")?;
    stream
        .set_write_timeout(Some(CANDIDATE_TIMEOUT))
        .context("Failed to set write timout")?;
    Ok(stream)
}

/**
 * Create a electrum RPC request
 */
pub fn electrum_request(id: i64, method: &str, params: Value) -> Vec<u8> {
    let mut req = json!({
        "id": id,
        "method": method,
        "params": params,
    })
    .to_string();
    req.push('\n');
    trace!("==> {}", req);
    req.as_bytes().to_vec()
}
