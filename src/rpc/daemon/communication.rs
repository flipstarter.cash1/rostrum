use anyhow::Result;
use async_trait::async_trait;
use serde_json::Value;
use tokio::sync::mpsc::Sender;

use super::Message;

#[async_trait]
pub trait Communcation: Send + Sync {
    /// Sending messages to the client
    async fn send_values(&mut self, values: &[Value]) -> Result<()>;

    /// Feed rpc queue with requests from client
    fn start_request_receiver(&mut self, rpc_queue_sender: Sender<Message>, idle_timeout: u64);

    /// End connection
    async fn shutdown(&mut self);
}
