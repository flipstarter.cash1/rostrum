use std::sync::Arc;

use crate::chaindef::OutPointHash;
use crate::chaindef::ScriptHash;
use crate::chaindef::TokenID;
use crate::chaindef::Transaction;
use crate::indexes::heightindex::HeightIndexRow;
use crate::indexes::inputindex::InputIndexRow;
use crate::indexes::outputindex::OutputIndexRow;
use crate::indexes::outputtokenindex::OutputTokenIndexRow;
use crate::indexes::scripthashindex::QueryFilter;
use crate::indexes::scripthashindex::ScriptHashIndexRow;
use crate::indexes::DBRow;
use crate::mempool::MEMPOOL_HEIGHT;
use crate::query::tx::TxQuery;
use crate::store::{DBStore, Row};
use anyhow::Result;
use bitcoin_hashes::Hash;
use bitcoincash::hash_types::Txid;
use futures_util::stream;
use futures_util::StreamExt;

pub(crate) async fn get_row<T: DBRow>(store: &DBStore, key: Vec<u8>) -> Option<T> {
    let (key, value) = store.get(T::CF, key).await;
    let row = Row {
        key: key.into_boxed_slice(),
        value: value?.into_boxed_slice(),
    };
    Some(T::from_row(&row))
}

/**
 * Fetche a single row found by key prefix.
 */
pub(crate) async fn get_row_by_key_prefix<T: DBRow>(store: &DBStore, key: Vec<u8>) -> Option<T> {
    let mut iter = store.scan(T::CF, key).await;
    iter.next().await.map(|r| T::from_row(&r))
}

/**
 * Find transaction height given a txid
 */
pub(crate) async fn height_by_txid(store: &DBStore, txid: &Txid) -> Option<u32> {
    let key = txid.to_vec();
    let (_key, value) = store.get(HeightIndexRow::CF, key).await;
    let height = HeightIndexRow::height_from_dbvalue(&value?);
    if height == MEMPOOL_HEIGHT {
        None
    } else {
        Some(height)
    }
}

/**
 * Collect all utxos sent to a scripthash.
 */
#[allow(clippy::needless_lifetimes)]
pub(crate) async fn outputs_by_scripthash<'a>(
    store: &'a DBStore,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> impl futures::stream::Stream<Item = OutputIndexRow> + 'a {
    let filter = ScriptHashIndexRow::filter_by_scripthash(scripthash.into_inner(), &filter);

    // Extract the outpoint hashes first
    let outpoint_hashes: Vec<OutPointHash> = store
        .scan(ScriptHashIndexRow::CF, filter)
        .await
        .map(|r| ScriptHashIndexRow::from_row(&r).outpointhash())
        .collect()
        .await;

    // Then use `stream::unfold` to process each `outpoint_hash`
    stream::unfold(outpoint_hashes.into_iter(), move |mut hashes| async move {
        if let Some(outpoint_hash) = hashes.next() {
            let rows_stream = store
                .scan(
                    OutputIndexRow::CF,
                    OutputIndexRow::filter_by_outpointhash(&outpoint_hash),
                )
                .await
                .map(|r| OutputIndexRow::from_row(&r));

            Some((rows_stream, hashes))
        } else {
            None
        }
    })
    .flatten()
}

pub(crate) async fn get_txheight_by_id(store: &DBStore, txid: &Txid) -> Option<HeightIndexRow> {
    let key = HeightIndexRow::filter_by_txid(txid);
    let (key, value) = store.get(HeightIndexRow::CF, key).await;
    Some(HeightIndexRow::from_row(&Row {
        key: key.into_boxed_slice(),
        value: value?.into_boxed_slice(),
    }))
}

/**
 * If token_id is provided, filters result by token ID.
 */
#[allow(clippy::needless_lifetimes)]
pub(crate) async fn txheight_by_scripthash(
    store: &DBStore,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> Vec<HeightIndexRow> {
    outputs_by_scripthash(store, scripthash, filter)
        .await
        .map(|output| output.txid())
        .then(|txid| async move {
            get_txheight_by_id(store, &txid)
                .await
                .unwrap_or_else(|| panic!("expected tx {} to have a height", txid))
        })
        .collect()
        .await
}

/**
 * Get a single output defined by its outpointhash HASH(txid, n)
 */
pub(crate) async fn get_utxo(
    store: &DBStore,
    outpointhash: &OutPointHash,
) -> Option<OutputIndexRow> {
    let key = outpointhash.to_vec();
    let (key, value) = store.get(OutputIndexRow::CF, key).await;

    Some(OutputIndexRow::from_row(&Row {
        key: key.into_boxed_slice(),
        value: value?.into_boxed_slice(),
    }))
}

pub(crate) async fn token_from_outpoint(
    store: &DBStore,
    outpointhash: &OutPointHash,
) -> Option<(TokenID, i64)> {
    let key = OutputTokenIndexRow::filter_by_outpointhash(outpointhash);
    match get_row_by_key_prefix::<OutputTokenIndexRow>(store, key).await {
        Some(token_row) => {
            let amount = token_row.token_amount();
            Some((token_row.into_token_id(), amount))
        }
        None => None,
    }
}

pub(crate) async fn get_tx_spending_prevout(
    store: &DBStore,
    txquery: &TxQuery,
    outpoint: &OutPointHash,
) -> Result<
    Option<(
        Transaction,
        u32, /* input index */
        u32, /* confirmation height */
    )>,
> {
    let spender = tx_spending_outpoint(store, outpoint).await;
    if spender.is_none() {
        return Ok(None);
    };
    let spender = spender.unwrap();
    let txid = spender.txid();
    let height = height_by_txid(store, &txid).await;
    let tx = txquery.get(&txid, None, height, false).await?;
    Ok(Some((tx, spender.index(), height.unwrap_or_default())))
}

pub(crate) async fn output_is_spent(store: &Arc<DBStore>, outpoint: OutPointHash) -> bool {
    store
        .exists_32bit_key(InputIndexRow::CF, outpoint.into_inner())
        .await
}

pub(crate) async fn tx_spending_outpoint(
    store: &DBStore,
    outpoint: &OutPointHash,
) -> Option<InputIndexRow> {
    let key = InputIndexRow::filter_by_outpointhash(outpoint);
    let (key, value) = store.get(InputIndexRow::CF, key).await;
    Some(InputIndexRow::from_row(&Row {
        key: key.into_boxed_slice(),
        value: value?.into_boxed_slice(),
    }))
}

pub async fn get_tx_funding_prevout(
    store: &DBStore,
    txquery: &TxQuery,
    outpoint: &OutPointHash,
) -> Result<
    Option<(
        Transaction,
        u32, /* input index */
        u32, /* confirmation height */
    )>,
> {
    let spender = tx_funding_outpoint(store, outpoint).await;
    if spender.is_none() {
        return Ok(None);
    };
    let spender = spender.unwrap();
    let txid = spender.txid();
    let height = height_by_txid(store, &txid).await;
    let tx = txquery.get(&txid, None, height, false).await?;
    Ok(Some((tx, spender.index(), height.unwrap_or_default())))
}

/**
 * Locate transaction that funded given output (created this utxo)
 */
pub async fn tx_funding_outpoint(
    store: &DBStore,
    outpoint: &OutPointHash,
) -> Option<OutputIndexRow> {
    let key = OutputIndexRow::filter_by_outpointhash(outpoint);
    let (key, value) = store.get(OutputIndexRow::CF, key).await;
    Some(OutputIndexRow::from_row(&Row {
        key: key.into_boxed_slice(),
        value: value?.into_boxed_slice(),
    }))
}
