use futures_util::{stream, StreamExt};
use std::{
    cmp::Ordering,
    collections::{HashMap, HashSet},
    sync::Arc,
};

use bitcoin_hashes::{hex::ToHex, Hash};
use bitcoincash::Txid;
use rayon::prelude::*;
use sha1::Digest;
use sha2::Sha256;

use crate::{
    chaindef::{OutPointHash, ScriptHash, TokenID},
    indexes::{
        outputindex::OutputIndexRow,
        scripthashindex::{QueryFilter, ScriptHashIndexRow},
        DBRow,
    },
    mempool::{ConfirmationState, Tracker},
    query::{
        queryutil::{get_utxo, token_from_outpoint},
        BUFFER_SIZE,
    },
    store::{DBContents, DBStore},
};

use super::queryutil::{height_by_txid, output_is_spent, tx_spending_outpoint};

#[derive(Serialize)]
pub struct HistoryItem {
    pub tx_hash: Txid,
    pub height: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fee: Option<u64>, // need to be set only for unconfirmed transactions (i.e. height <= 0)
}

#[derive(Serialize)]
pub struct UnspentItem {
    #[serde(rename = "tx_hash")]
    pub txid: Txid,
    #[serde(rename = "tx_pos")]
    pub vout: u32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub outpoint_hash: Option<OutPointHash>,
    pub height: u32,
    pub value: u64,

    #[cfg(feature = "nexa")]
    #[serde(skip_serializing_if = "Option::is_none", rename = "token_id_hex")]
    pub token_id: Option<TokenID>,

    #[cfg(not(feature = "nexa"))]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub token_id: Option<TokenID>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub token_amount: Option<i64>,

    /// If this utxo has token in it. Useful when not querying for token info (which adds query cost).
    pub has_token: bool,
}

/**
 * For sorting history items by confirmation height (and then ID)
 */
pub(crate) fn by_block_height(a: &HistoryItem, b: &HistoryItem) -> Ordering {
    if a.height == b.height {
        // Order by little endian tx hash if height is the same,
        // in most cases, this order is the same as on the blockchain.
        return b.tx_hash.cmp(&a.tx_hash);
    }
    if a.height > 0 && b.height > 0 {
        return a.height.cmp(&b.height);
    }

    // mempool txs should be sorted last, so add to it a large number
    // per spec, mempool entries do not need to be sorted, but we do it
    // anyway so that statushash is deterministic
    let mut a_height = a.height;
    let mut b_height = b.height;
    if a_height <= 0 {
        a_height = 0xEE_EEEE + a_height.abs();
    }
    if b_height <= 0 {
        b_height = 0xEE_EEEE + b_height.abs();
    }
    a_height.cmp(&b_height)
}

/**
 * Find output rows given filter parameters.
 *
 * This is an expensive call used by  most blockchain.scripthash.* queries.
 *
 * Returns the utxos + bool indicating if utxo has token
 */
async fn scan_for_outputs<'a>(
    store: &'a Arc<DBStore>,
    scripthash: ScriptHash,
    filter: &'a QueryFilter,
    filter_token: &'a Option<TokenID>,
) -> impl futures::stream::Stream<Item = (OutputIndexRow, bool)> + 'a {
    let scripthash_filter =
        ScriptHashIndexRow::filter_by_scripthash(scripthash.into_inner(), filter);

    let dummy_hash: OutPointHash = OutPointHash::all_zeros();

    // let store_cpy = Arc::clone(store);
    store
        .scan(ScriptHashIndexRow::CF, scripthash_filter)
        .await
        .map(|r| ScriptHashIndexRow::from_row(&r))
        .then(move |r| {
            // let store = Arc::clone(&store);
            async move {
                (
                    get_utxo(store, &r.outpointhash())
                        .await
                        .expect("missing utxo"),
                    r.has_token(),
                )
            }
        })
        .filter(move |(o, _)| {
            // Extract the hash before entering the async block.
            let hash = if filter_token.is_some() {
                o.hash()
            } else {
                dummy_hash
            };

            async move {
                if let Some(filter) = &filter_token {
                    match token_from_outpoint(store, &hash).await {
                        Some((t, _)) => &t == filter,
                        None => false,
                    }
                } else {
                    true
                }
            }
        })
}

/**
 * Calculate coin balance in scripthash that has been confirmed in blocks.
 */
pub async fn confirmed_scripthash_balance(
    index: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> (i64, Vec<(i64, OutPointHash)>) {
    assert!(index.contents == DBContents::ConfirmedIndex);
    let outputs: Vec<(i64, OutPointHash)> = scan_for_outputs(index, scripthash, &filter, &None)
        .await
        .map(|(o, _)| (o.value(), o.take_hash()))
        .then(|(value, outpoint)| {
            let index = Arc::clone(index);
            async move {
                if output_is_spent(&index, outpoint).await {
                    // Output was created AND spent in the same index. Zero it out.
                    (0, outpoint)
                } else {
                    (value as i64, outpoint)
                }
            }
        })
        .map(|x| async move { x })
        .buffer_unordered(BUFFER_SIZE)
        .collect()
        .await;

    let amount = outputs.par_iter().map(|(value, _)| value).sum();

    // Only return unspent confirmed outputs
    let outputs = outputs
        .into_iter()
        .filter(|(amount, _)| amount > &0)
        .collect();

    (amount, outputs)
}

/**
 * Calculate coin balance in scripthash that has been confirmed in blocks.
 *
 * Takes confirmed_outputs in to be able to see if mempool spends confirmed utxos.
 */
pub async fn unconfirmed_scripthash_balance(
    mempool: &Arc<DBStore>,
    confirmed_outputs: Vec<(i64, OutPointHash)>,
    scripthash: ScriptHash,
    filter: QueryFilter,
) -> i64 {
    assert!(DBContents::MempoolIndex == mempool.contents);

    let unconfirmed_outputs: Vec<i64> = scan_for_outputs(mempool, scripthash, &filter, &None)
        .await
        .map(|(o, _)| (o.value(), o.take_hash()))
        .map(|(value, outpoint)| async move {
            if output_is_spent(mempool, outpoint).await {
                // Output was created AND spent in the same index. Zero it out.
                0
            } else {
                value as i64
            }
        })
        .buffer_unordered(BUFFER_SIZE)
        .collect()
        .await;

    let amount: i64 = unconfirmed_outputs.par_iter().sum();

    // Subtract spends from confirmed utxos
    let spent_confirmed: i64 = stream::iter(confirmed_outputs)
        .map(|(value, out)| async move {
            assert!(value >= 0);
            if output_is_spent(mempool, out).await {
                value
            } else {
                0
            }
        })
        .buffer_unordered(BUFFER_SIZE)
        .fold(0i64, |acc, value| async move { acc + value })
        .await;

    amount - spent_confirmed
}

// Used in .reduce for merging maps
fn merge_token_balance_maps(
    mut a: HashMap<TokenID, i64>,
    b: HashMap<TokenID, i64>,
) -> HashMap<TokenID, i64> {
    for (token_id, amount) in b {
        a.entry(token_id)
            .and_modify(|a| {
                *a += amount;
            })
            .or_insert(amount);
    }
    a
}

/**
 * Calculate token balance in a given store.
 */
async fn calc_token_balance(
    store: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter_token: Option<TokenID>,
) -> (HashMap<TokenID, i64>, Vec<OutPointHash>) {
    let (balance, outputs) =
        scan_for_outputs(store, scripthash, &QueryFilter::TokenOnly, &filter_token)
            .await
            .map(|(o, _)| o.take_hash())
            .map(|x| async move { x })
            .buffer_unordered(BUFFER_SIZE)
            .then(|outpoint| async move {
                let (token_id, amount) = token_from_outpoint(store, &outpoint)
                    .await
                    .expect("token info found for outpoint {outpoint}");
                let valid_amount = if amount < 0 || output_is_spent(store, outpoint).await {
                    0
                } else {
                    amount
                };
                (token_id, valid_amount, outpoint)
            })
            .fold(
                (HashMap::<TokenID, i64>::default(), Vec::new()),
                |(mut map, mut ops), (token_id, amount, outpoint)| async move {
                    map.entry(token_id)
                        .and_modify(|a| *a += amount)
                        .or_insert(amount);
                    ops.push(outpoint);
                    (map, ops)
                },
            )
            .await;

    (balance, outputs)
}

/**
 * Get the unconfirmed token balance
 */
pub(crate) async fn unconfirmed_scripthash_token_balance(
    mempool: &Arc<DBStore>,
    index: &Arc<DBStore>,
    confirmed_outputs: Vec<OutPointHash>,
    scripthash: ScriptHash,
    filter_token: Option<TokenID>,
) -> HashMap<TokenID, i64> {
    assert!(mempool.contents == DBContents::MempoolIndex);
    assert!(index.contents == DBContents::ConfirmedIndex);

    // This finds the balance of entries that have been both created and spent
    // in the mempool. It cannot see spends of outputs thave have been confirmed.
    let (balance, _) = calc_token_balance(mempool, scripthash, filter_token).await;

    // Find inputs that spends from confirmed outputs as well
    #[allow(clippy::redundant_closure)]
    let spends_of_confirmed = stream::iter(confirmed_outputs)
        .then(|outpoint| async move {
            if output_is_spent(mempool, outpoint).await {
                let (token_id, amount) = token_from_outpoint(index, &outpoint)
                    .await
                    .expect("token info found for outpoint {outpoint}");
                (token_id, -amount)
            } else {
                // This output was funded in the confirmed index, not mempool.
                // If it's not spent here, then ignore it.
                (TokenID::all_zeros(), 0)
            }
        })
        .fold(
            HashMap::<TokenID, i64>::default(),
            |mut map, (token_id, amount)| async move {
                map.entry(token_id)
                    .and_modify(|a| {
                        *a += amount;
                    })
                    .or_insert(amount);
                map
            },
        )
        .await;

    // Merge in spends of confirmed outputs
    let mut balance = merge_token_balance_maps(balance, spends_of_confirmed);
    balance.remove(&TokenID::all_zeros());

    balance
}

/**
 * Get the confirmed token balance
 */
pub(crate) async fn confirmed_scripthash_token_balance(
    store: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter_token: Option<TokenID>,
) -> (HashMap<TokenID, i64>, Vec<OutPointHash>) {
    calc_token_balance(store, scripthash, filter_token).await
}

/**
 * Find all transactions that spend of fund scripthash.
 *
 * Parameter 'additional_outpoints' is for funding utxos from other indexes.
 * For mempool, this parameter is needed to be able to locate spends of confirmed outputs
 * (as those outputs are not funded in the mempool)
 */
pub(crate) async fn scripthash_transactions(
    store: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter: QueryFilter,
    filter_token: Option<TokenID>,
    additional_outputs: Vec<OutPointHash>,
) -> (HashSet<Txid>, Vec<OutPointHash>) {
    let outputs = scan_for_outputs(store, scripthash, &filter, &filter_token).await;

    let (mut funder_txid, outpoints): (HashSet<Txid>, Vec<OutPointHash>) = outputs
        .map(|(o, _)| async move { (o.txid(), o.hash()) })
        .buffer_unordered(BUFFER_SIZE)
        .unzip()
        .await;

    let spender_txids: HashSet<Txid> = stream::iter(outpoints.iter())
        .chain(stream::iter(additional_outputs.iter()))
        .filter_map(|o| async move {
            tx_spending_outpoint(store, o)
                .await
                .map(|input| input.txid())
        })
        .collect()
        .await;

    funder_txid.extend(spender_txids);
    (funder_txid, outpoints)
}

/**
 * Get outputs that have been confirmed on the blockchain given filter parameters.
 */
pub(crate) async fn get_confirmed_outputs(
    index: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter: QueryFilter,
    filter_token: Option<TokenID>,
) -> Vec<OutPointHash> {
    assert!(index.contents == DBContents::ConfirmedIndex);

    scan_for_outputs(index, scripthash, &filter, &filter_token)
        .await
        .map(|(o, _)| async move { o.hash() })
        .buffer_unordered(BUFFER_SIZE)
        .collect()
        .await
}

/**
 * Generate list of HistoryItem for a scripthashes confirmed history.
 *
 * Also returns confirmed outputs that can be used to get unconfirmed spends
 * of confirmed utxos.
 */
async fn confirmed_history(
    store: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter: QueryFilter,
    filter_token: Option<TokenID>,
) -> (Vec<HistoryItem>, Vec<OutPointHash>) {
    let (txids, outputs) =
        scripthash_transactions(store, scripthash, filter, filter_token, vec![]).await;

    let history = stream::iter(txids)
        .map(|txid| async move {
            let height = match height_by_txid(store, &txid).await {
                Some(h) => h,
                None => {
                    debug_assert!(false, "Confirmed tx cannot be missing height");
                    0
                }
            };
            HistoryItem {
                tx_hash: txid,
                height: height as i32,
                fee: None,
            }
        })
        .buffer_unordered(BUFFER_SIZE)
        .collect()
        .await;
    (history, outputs)
}

/**
 * Generate list of HistoryItem for a scripthashes unconfirmed history
 */
pub async fn unconfirmed_history(
    mempool: &Tracker,
    confirmed_outputs: Vec<OutPointHash>,
    scripthash: ScriptHash,
    filter: QueryFilter,
    filter_token: Option<TokenID>,
) -> Vec<HistoryItem> {
    let (txids, _) = scripthash_transactions(
        mempool.index(),
        scripthash,
        filter,
        filter_token,
        confirmed_outputs,
    )
    .await;

    stream::iter(txids)
        .map(|txid| async move {
            let height = match mempool.tx_confirmation_state(&txid, None).await {
                ConfirmationState::InMempool => 0,
                ConfirmationState::UnconfirmedParent => -1,
                ConfirmationState::Indeterminate | ConfirmationState::Confirmed => {
                    debug_assert!(
                        false,
                        "Mempool tx's state cannot be indeterminate or confirmed"
                    );
                    0
                }
            };
            HistoryItem {
                tx_hash: txid,
                height,
                fee: mempool.get_fee(&txid).await,
            }
        })
        .buffer_unordered(BUFFER_SIZE)
        .collect()
        .await
}

pub async fn scripthash_history(
    store: &Arc<DBStore>,
    mempool: &Tracker,
    scripthash: ScriptHash,
    filter: QueryFilter,
    filter_token: Option<TokenID>,
) -> Vec<HistoryItem> {
    let (mut history, confirmed_outputs) =
        confirmed_history(store, scripthash, filter, filter_token.clone()).await;

    history.extend(
        unconfirmed_history(mempool, confirmed_outputs, scripthash, filter, filter_token).await,
    );

    tokio::task::spawn_blocking(|| async move {
        history.par_sort_unstable_by(by_block_height);
        history
    })
    .await
    .unwrap()
    .await
}

/**
 * Generate a hash of scripthash history as defined in electrum spec
 */
pub fn hash_scripthash_history(history: &Vec<HistoryItem>) -> Option<[u8; 32]> {
    if history.is_empty() {
        None
    } else {
        let mut sha2 = Sha256::new();
        let parts: Vec<String> = history
            .into_par_iter()
            .map(|t| format!("{}:{}:", t.tx_hash.to_hex(), t.height))
            .collect();

        for p in parts {
            sha2.update(p.as_bytes());
        }
        Some(sha2.finalize().into())
    }
}

pub(crate) async fn scripthash_listunspent(
    store: &Arc<DBStore>,
    mempool: &Arc<DBStore>,
    scripthash: ScriptHash,
    filter: QueryFilter,
    filter_token: Option<TokenID>,
) -> Vec<UnspentItem> {
    assert!(store.contents == DBContents::ConfirmedIndex);
    assert!(mempool.contents == DBContents::MempoolIndex);

    let confirmed = scan_for_outputs(store, scripthash, &filter, &filter_token)
        .await
        .filter(|(out, _)| {
            let outpoint = out.hash();
            let store = Arc::clone(store);
            async move { !output_is_spent(&store, outpoint).await }
        })
        .filter(|(out, _)| {
            let outpoint = out.hash();
            let mempool = Arc::clone(mempool);
            async move { !output_is_spent(&mempool, outpoint).await }
        })
        .map(|(out, has_token)| {
            let store = Arc::clone(store);
            async move {
                let txid = out.txid();

                (
                    out,
                    height_by_txid(&store, &txid).await.expect("height missing"),
                    has_token,
                )
            }
        })
        .buffer_unordered(BUFFER_SIZE);

    let unconfirmed = scan_for_outputs(mempool, scripthash, &filter, &filter_token)
        .await
        .filter(|(out, _)| {
            let outpoint = out.hash();
            let mempool = Arc::clone(mempool);
            async move { !output_is_spent(&mempool, outpoint).await }
        })
        .map(|(out, has_token)| {
            (out, 0 /* mempool height */, has_token)
        });

    let unspent_outputs = confirmed.chain(unconfirmed);

    // Fetch token info
    let unspent_outputs = unspent_outputs.filter_map(|(output, height, has_token)| {
        let filter_token =
            if has_token && filter == QueryFilter::TokenOnly && filter_token.is_some() {
                filter_token.clone()
            } else {
                None
            };
        let store = Arc::clone(store);
        let mempool = Arc::clone(mempool);

        async move {
            // If explicitly querying for tokens, include token info.
            if has_token && filter == QueryFilter::TokenOnly {
                let (token_id, amount) = match token_from_outpoint(&store, &output.hash()).await {
                    Some(result) => result,
                    None => token_from_outpoint(&mempool, &output.hash())
                        .await
                        .expect("missing token info"),
                };

                if let Some(filter) = &filter_token {
                    if filter == &token_id {
                        Some((output, height, Some(token_id), Some(amount), has_token))
                    } else {
                        None
                    }
                } else {
                    Some((output, height, Some(token_id), Some(amount), has_token))
                }
            } else {
                Some((output, height, None, None, has_token))
            }
        }
    });

    // Collect into json-serializable objects
    let unspent_outputs =
        unspent_outputs.map(|(out, height, token_id, token_amount, has_token)| {
            #[cfg(feature = "nexa")]
            {
                // With nexa, we also want outpoint hash
                UnspentItem {
                    txid: out.txid(),
                    vout: out.index(),
                    outpoint_hash: Some(out.hash()),
                    height,
                    value: out.value(),
                    token_id,
                    token_amount,
                    has_token,
                }
            }
            #[cfg(not(feature = "nexa"))]
            {
                UnspentItem {
                    txid: out.txid(),
                    vout: out.index(),
                    outpoint_hash: None,
                    height,
                    value: out.value(),
                    token_id,
                    token_amount,
                    has_token,
                }
            }
        });

    let mut unspent: Vec<UnspentItem> = unspent_outputs.collect().await;

    unspent.par_sort_by_key(|u| u.height);

    unspent
}
