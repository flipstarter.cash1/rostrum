#!/usr/bin/env python3
"""
Build documentation
"""
import os
import shutil

script_dir = os.path.dirname(os.path.realpath(__file__))
os.chdir(script_dir)
os.system('python3 config-toml-to-md.py > ../doc/configuration.md')
os.chdir(os.path.join(script_dir, ".."))
shutil.copy('README.md', os.path.join('doc', 'index.md'))
os.system('mkdocs build')
